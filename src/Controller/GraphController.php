<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use CMEN\GoogleChartsBundle\GoogleCharts\Charts\Material\ColumnChart;

class GraphController extends AbstractController
{
    
    public function columnChart($data, $questionName)
    {
        $chart = new ColumnChart();
      $chart->getData()->setArrayToDataTable($data);
      $chart->getOptions()->getChart()->setTitle($questionName);
      $chart->getOptions()
          ->setBars('vertical')
          ->setHeight(200)
          ->setWidth(700)
          ->setColors(['#0066ff'])
          ->getVAxis()
          ->setFormat('decimal');
        return $chart;

    
    }

    /**
     * @Route("/graph2", name="graph2")
     */
    public function graph2()
    {
        $org = new OrgChart();
        $org->getData()->setArrayToDataTable(
            [
                [['v' => 'Mike', 'f' => 'Mike<div style="color:red; font-style:italic">President</div>'], '', 'The President'],
                [['v' => 'Jim', 'f' => 'Jim<div style="color:red; font-style:italic">Vice President</div>'], 'Mike', 'VP'],
                ['Alice', 'Mike', ''],
                ['Bob', 'Jim', 'Bob Sponge'],
                ['Carol', 'Bob', '']
            ],
            true
        );
        $org->getOptions()->setAllowHtml(true);

        return $this->render('graph/graph.html.twig', [
            'title' => 'Org Chart',
            'chart' => $org
        ]);
    }

    /**
     * @Route("/graph3", name="graph3")
     */
    public function graph3()
    {
        $combo = new ComboChart();
        $combo->getData()->setArrayToDataTable([
            ['Month', 'Bolivia', 'Ecuador', 'Madagascar', 'Papua New Guinea', 'Rwanda', 'Average'],
            ['2004/05',  165,      938,         522,             998,           450,      614.6],
            ['2005/06',  135,      1120,        599,             1268,          288,      682],
            ['2006/07',  157,      1167,        587,             807,           397,      623],
            ['2007/08',  139,      1110,        615,             968,           215,      609.4],
            ['2008/09',  136,      691,         629,             1026,          366,      569.6]
        ]);
        $combo->getOptions()->setTitle('Monthly Coffee Production by Country');
        $combo->getOptions()->getVAxis()->setTitle('Cups');
        $combo->getOptions()->getHAxis()->setTitle('Month');
        $combo->getOptions()->setSeriesType('bars');

        $series5 = new Series();
        $series5->setType('line');
        $combo->getOptions()->setSeries([5 => $series5]);

        $combo->getOptions()->setWidth(900);
        $combo->getOptions()->setHeight(500);

        return $this->render('graph/graph.html.twig', [
            'title' => 'Combo',
            'chart' => $combo
        ]);
    }

    /**
     * @Route("/graph4", name="graph4")
     */
    public function graph4()
    {
        $chart = new ColumnChart();
        $chart->getData()->setArrayToDataTable([
            ['Year', 'Sales', 'Expenses', 'Profit'],
            ['2014', 1000, 400, 200],
            ['2015', 1170, 460, 250],
            ['2016', 660, 1120, 300],
            ['2017', 1030, 540, 350]
        ]);
        $chart->getOptions()->getChart()
            ->setTitle('Company Performance')
            ->setSubtitle('Sales, Expenses, and Profit: 2014-2017');
        $chart->getOptions()
            ->setBars('vertical')
            ->setHeight(400)
            ->setWidth(900)
            ->setColors(['#1b9e77', '#d95f02', '#7570b3'])
            ->getVAxis()
            ->setFormat('decimal');

        return $this->render('graph/graph.html.twig', [
            'title' => 'Column Chart',
            'chart' => $chart
        ]);
    }
}