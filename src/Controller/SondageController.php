<?php

namespace App\Controller;

use App\Entity\Sondage;
use App\Entity\Question;
use App\Entity\Reponse;
use App\Entity\Feedback;
use App\Form\SondageType;
use App\Repository\SondageRepository;
use App\Controller\GraphController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SondageController extends AbstractController
{
    private $graphController;

    public function __construct() {
      $this->graphController = new GraphController();
    }
    /**
     * @Route("", name="sondage_index", methods={"GET"})
     */
    public function index(SondageRepository $sondageRepository): Response
    {
        return $this->render('sondage/index.html.twig', [
            'sondages' => $sondageRepository->findAll(),
        ]);
    }

    

    /**
     * @Route("/{graph}/{id}", name="sondage_result", methods={"GET"})
     */
    public function result($graph, Sondage $sondage): Response
    {

        $questions = $this->getDoctrine()->getRepository(Question::class)->createQueryBuilder('a')
          ->where('a.sondage = ' . $sondage->getId())
          ->select('a.nom, a.id')->getQuery()->getResult();
        
        $charts = [];
        foreach ($questions as $question) {
          $responses = $this->getDoctrine()->getRepository(Reponse::class)->createQueryBuilder('a')
            ->where('a.question = ' . $question["id"])
            ->select('a.note')->getQuery()->getResult();

          $responses = array_column($responses, 'note');
          $responses = array_count_values($responses);
          $data = [['Réponse sur une échelle de 1 à 6','Nombre de réponses']];
          foreach ($responses as $key => $value) {
            array_push($data, [$key, $value]);
          }
          $chart = $this->graphController->$graph($data, $question["nom"]);
          array_push($charts, ['res' => $chart, 'question' => strval($question["id"])]);
        }

        return $this->render('sondage/result.html.twig', [
            'sondage' => $sondage,
            'charts' => $charts
        ]);
    }

    /**
     * @Route("/feedback/{id}", name="feedback", methods={"POST"})
     */
    public function feedback(Feedback $feedback): Response
    {
      return $this->render('feedback.html.twig', []);
    }

    
}
