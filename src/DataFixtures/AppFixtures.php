<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Sondage;
use App\Entity\Question;
use App\Entity\Feedback;
use App\Entity\Reponse;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        //sondage
        $sondage = new Sondage();
        $sondage->setNom('Sondage Test');
        $sondage->setDestinataire('test1@test.ch,test2@test.ch,test3@test.ch');
        $manager->persist($sondage);

        //question
        $question = new Question();
        $question->setNom('Je comprends ce que je fais');
        $question->setSondage($sondage);
        $manager->persist($question);

        $question = new Question();
        $question->setNom('Je fuis le covid');
        $question->setSondage($sondage);
        $manager->persist($question);

        $question = new Question();
        $question->setNom('Je mange le covid');
        $question->setSondage($sondage);
        $manager->persist($question);

        $manager->flush();
        $manager->refresh($sondage);

    
         //feedback
         $mails = explode(',', $sondage->getDestinataire());
         foreach($mails as $mail){
             $feedback = new Feedback();
             $feedback->setEmail($mail);
             $feedback->setSondage($sondage);
             $manager->persist($feedback);

             foreach($sondage->getQuestions() as $question){
                $note = rand(1,6);
                $reponse = new Reponse();
                $reponse->setNote($note);
                $reponse->setFeedback($feedback);
                $reponse->setQuestion($question);
                $manager->persist($reponse);
             }
         }

        $manager->flush();
    }
}
